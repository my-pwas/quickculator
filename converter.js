let conversionTypes = ["area", "length", "volume", "mass", "temperature", "speed", "time", "data"];
let conversionType = "area";
let convertFrom = "acres";
let convertTo = "square meters";

const changeConversionType = () => {
    for (let i in conversionTypes) {
        document.getElementById(conversionTypes[i]).addEventListener("click", (e) => {
            conversionType = conversionTypes[i];
            e.target.style.backgroundColor = "#fafafa";
            e.target.style.color = "darkblue";
            for (let j in conversionTypes) {
                if (j != i) {
                    document.getElementById(conversionTypes[j]).style.backgroundColor = "transparent";
                    document.getElementById(conversionTypes[j]).style.color = "#fafafa";
                }
            }
            console.log(conversionType);
        })
    }
}; changeConversionType();

/* area */
let acres;
const squareMetersValue = () => { if (acres !== (null || undefined)) { acres * 4046.8564224 } };

document.getElementById("converter-input-1").addEventListener("input", (e) => {
    acres = e.target.value;
    document.getElementById("converter-output-2").innerHTML = squareMetersValue();

    console.log(acres);
    console.log(squareMeters);
});